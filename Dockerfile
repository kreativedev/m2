FROM ubuntu:16.04

MAINTAINER Superior Threads <it@superiorthreads.com>

RUN apt-get update && apt-get install -y curl git supervisor nginx php7.0-fpm php7.0-mcrypt php7.0-curl php7.0-cli php7.0-mysql php7.0-gd php7.0-xsl php7.0-json php7.0-intl php-pear php7.0-dev php7.0-common php7.0-mbstring php7.0-zip php-soap
RUN  sed -i "s|memory_limit = 128M|memory_limit = 1024M |g" /etc/php/7.0/fpm/php.ini

RUN mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default_org
ADD ./default /etc/nginx/sites-available/default
ADD ./supervisor-magento2.conf /etc/supervisor/conf.d/supervisor-superiormagento.conf

RUN git clone https://github.com/magento/magento2.git /var/www/magento2 && \
    mkdir -p /var/run/php && \
    mkdir -p /var/log/php-fpm && \
    curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/bin/composer && \
    chmod 700 /var/www/magento2/app/etc && \
    chown -R www-data:www-data /var/www/magento2

RUN echo "daemon off;" >> /etc/nginx/nginx.conf
CMD ["supervisord", "-n"]
